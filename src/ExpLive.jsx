import React from 'react';
import { connect } from 'react-redux'

class Live extends React.Component {
    
    render() {
        return <div className='box'>
            <p>Latitud: {this.props.map.latitude}</p>
            <p>Longitud: {this.props.map.longitude}</p>
            <p>Luminescencia: {this.props.blue.split('#')[0]}</p>
            <p>Radiación UV: {this.props.blue.split('#')[1]}</p>
        </div>;
    }
}

const mapper = (state) => {
    return {
        map: state.fast.mapData || {},
        blue: state.fast.blueData || '',
    }
}

export default connect(mapper)(Live)