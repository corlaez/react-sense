import React from 'react'
import { connect } from 'react-redux'
import { withGoogleMap, GoogleMap, Marker, Circle } from "react-google-maps"
const { DrawingManager } = require("react-google-maps/lib/components/drawing/DrawingManager");

function getColor(value){
    //value from 0 to 1
    var hue=((1-value)*120).toString(10);
    return ["hsl(",hue,",100%,50%)"].join("");
}

const MyMapComponent = withGoogleMap((props) => {
    const mapNow = window.store.getState().fast.mapData
    return <GoogleMap
        defaultZoom={17}
        defaultCenter={{ lat: mapNow.latitude, lng: mapNow.longitude }}
    >

        {/*<DrawingManager
            defaultDrawingMode={window.google.maps.drawing.OverlayType.CIRCLE}
            defaultOptions={{
                drawingControl: true,
                drawingControlOptions: {
                    position: window.google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: [
                        window.google.maps.drawing.OverlayType.CIRCLE,
                        window.google.maps.drawing.OverlayType.POLYGON,
                        window.google.maps.drawing.OverlayType.POLYLINE,
                        window.google.maps.drawing.OverlayType.RECTANGLE,
                        window.google.maps.drawing.OverlayType.MARKER,
                    ],
                },
                circleOptions: {
                    fillColor: `#ffff00`,
                    fillOpacity: 1,
                    strokeWeight: 5,
                    clickable: false,
                    editable: true,
                    zIndex: 1,
                },
            }}
        />*/}
        {/*props.markers.map(e => <Marker position={{ lat: e.latitude, lng: e.longitude }} />)*/}
        {props.markers.map(e => 
            <Circle key={Math.random() + '-' + e.timestamp} 
                options={{ 
                    fillColor: getColor(Number(e.blueData.split('#')[1])), //TODO
                    strokeColor: getColor(Number(e.blueData.split('#')[1])), 
                    clickable: true,
                    onClick: () => {
                        alert(
                            'Radiación UV: '+e.blueData.split('#')[1] + '\n',
                            'Lúmenes: '+e.blueData.split('#')[1] + '\n',
                            'Fecha y Hora: '+ new Date(e.timestamp),
                        );
                    },
                    zIndex: e.timestamp,
                    radius: 5,
                    fillOpacity: 1, 
                    strokeWeight: 5}
                }
                center={{ lat: e.mapData.latitude, lng: e.mapData.longitude }} />
        )}
    </GoogleMap>
})

class ExpVisual extends React.Component {
    render() {
        return <div><br/>
            <MyMapComponent
                markers={this.props.data}
                loadingElement={<div style={{ height: `100%` }} />}
                containerElement={<div style={{ height: `400px` }} />}
                mapElement={<div style={{ height: `100%` }} />}
            />
            {/*map(window.store.getState().fast.mapData)*/}
        </div>
    }
}

const mapper = state => {
    return {
        data: state.rec[state.rec.currentId] || []
    }
}

export default connect(mapper)(ExpVisual)