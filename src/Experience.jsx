import React from 'react';
import ExpLive from './ExpLive'
import ExpRecord from './ExpRecord'

class Experience extends React.Component {

    render() {
        return <div>
            <ExpLive/>
            <ExpRecord/>
        </div>;
    }
}

export default Experience;
